package vn.longnh.procuracy.mapper;

import org.mapstruct.Mapper;

import vn.longnh.procuracy.dto.request.DeviceInfoRequest;
import vn.longnh.procuracy.dto.response.DeviceInfoResponse;
import vn.longnh.procuracy.entity.DeviceInfo;

@Mapper(componentModel = "spring")
public abstract class DeviceInfoMapper extends BaseMapper<DeviceInfoRequest, DeviceInfoResponse, DeviceInfo> {}
