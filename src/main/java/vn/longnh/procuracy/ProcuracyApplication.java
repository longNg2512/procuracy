package vn.longnh.procuracy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProcuracyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProcuracyApplication.class, args);
    }
}
