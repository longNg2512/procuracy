package vn.longnh.procuracy.dto.response;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DeviceInfoResponse {
    Long id;

    String name;

    String deviceUuid;

    String status;

    LocalDateTime createdAt;

    LocalDateTime updatedAt;
}
