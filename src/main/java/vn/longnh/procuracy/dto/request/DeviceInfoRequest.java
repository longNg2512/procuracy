package vn.longnh.procuracy.dto.request;

import jakarta.validation.constraints.NotBlank;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DeviceInfoRequest {

    @NotBlank
    String name;

    @NotBlank
    String deviceUuid;
}
