package vn.longnh.procuracy.repository;

import vn.longnh.procuracy.entity.DeviceInfo;

public interface DeviceInfoRepository extends BaseRepository<DeviceInfo, Long> {}
