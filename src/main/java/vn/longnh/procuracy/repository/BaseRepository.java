package vn.longnh.procuracy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

@NoRepositoryBean
public interface BaseRepository<T, ID> extends JpaRepository<T, ID> {

    @Transactional
    @Modifying
    @Query("update #{#entityName} e set e.deletedAt = CURRENT_TIMESTAMP where e.id = ?1")
    void softDeleteById(ID id);
}
