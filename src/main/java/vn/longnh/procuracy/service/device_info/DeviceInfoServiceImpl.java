package vn.longnh.procuracy.service.device_info;

import org.springframework.stereotype.Service;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import vn.longnh.procuracy.dto.request.DeviceInfoRequest;
import vn.longnh.procuracy.dto.response.DeviceInfoResponse;
import vn.longnh.procuracy.entity.DeviceInfo;
import vn.longnh.procuracy.mapper.DeviceInfoMapper;
import vn.longnh.procuracy.repository.DeviceInfoRepository;
import vn.longnh.procuracy.service.base.BaseServiceImpl;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DeviceInfoServiceImpl extends BaseServiceImpl<DeviceInfoRequest, DeviceInfoResponse, DeviceInfo, Long>
        implements DeviceInfoService {

    DeviceInfoMapper mapper;
    DeviceInfoRepository repository;

    public DeviceInfoServiceImpl(DeviceInfoMapper mapper, DeviceInfoRepository repository) {
        super(mapper, repository);
        this.mapper = mapper;
        this.repository = repository;
    }
}
