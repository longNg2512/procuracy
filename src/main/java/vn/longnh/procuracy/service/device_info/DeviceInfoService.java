package vn.longnh.procuracy.service.device_info;

import vn.longnh.procuracy.dto.request.DeviceInfoRequest;
import vn.longnh.procuracy.dto.response.DeviceInfoResponse;
import vn.longnh.procuracy.service.base.BaseService;

public interface DeviceInfoService extends BaseService<DeviceInfoRequest, DeviceInfoResponse, Long> {}
