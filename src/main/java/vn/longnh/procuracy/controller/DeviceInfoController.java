package vn.longnh.procuracy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import vn.longnh.procuracy.dto.request.DeviceInfoRequest;
import vn.longnh.procuracy.dto.response.DeviceInfoResponse;
import vn.longnh.procuracy.service.device_info.DeviceInfoService;

@RestController
@RequestMapping("/api/v1/public/device-info")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DeviceInfoController
        extends BaseController<DeviceInfoRequest, DeviceInfoResponse, Long, DeviceInfoService> {

    public DeviceInfoController(DeviceInfoService service) {
        super(service);
        this.service = service;
    }
}
