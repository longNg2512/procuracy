package vn.longnh.procuracy.controller;

import java.util.List;

import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import vn.longnh.procuracy.dto.request.MassActionRequest;
import vn.longnh.procuracy.dto.response.ApiResponse;
import vn.longnh.procuracy.exception.ApiException;
import vn.longnh.procuracy.exception.ValidationException;
import vn.longnh.procuracy.service.base.BaseService;

public abstract class BaseController<Rq, Rp, ID, Sv extends BaseService<Rq, Rp, ID>> {

    protected Sv service;

    protected BaseController(Sv service) {
        this.service = service;
    }

    @PostMapping("")
    @Operation(summary = "Thêm mới")
    public ApiResponse<Rp> create(@RequestBody @Valid Rq rq) throws ValidationException {
        return ApiResponse.ok(service.insert(rq));
    }

    @PatchMapping("/{id}")
    @Operation(summary = "Cập nhật")
    public ApiResponse<Rp> update(@PathVariable ID id, @RequestBody @Valid Rq rq)
            throws ValidationException, ApiException {
        return ApiResponse.ok(service.update(id, rq));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Lấy chi tiết")
    public ApiResponse<Rp> get(@PathVariable ID id) {
        return ApiResponse.ok(service.getById(id));
    }

    @GetMapping("")
    @Operation(summary = "Lấy nhiều")
    public ApiResponse<List<Rp>> get(@RequestBody @Valid MassActionRequest<ID> rq) {
        return ApiResponse.ok(service.getByIds(rq.getIds()));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Xoá cứng")
    public ApiResponse<Rp> delete(@PathVariable ID id) throws ValidationException {
        return ApiResponse.ok(service.deleteById(id));
    }

    @DeleteMapping("")
    @Operation(summary = "Xoá cứng nhiều")
    public ApiResponse<List<Rp>> delete(@RequestBody @Valid MassActionRequest<ID> rq) throws ValidationException {
        return ApiResponse.ok(service.deleteByIds(rq.getIds()));
    }

    @PatchMapping("/soft-delete/{id}")
    @Operation(summary = "Xoá mềm")
    public ApiResponse<Rp> softDelete(@PathVariable ID id) throws ValidationException {
        return ApiResponse.ok(service.softDeleteById(id));
    }
}
